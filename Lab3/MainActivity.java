package com.brettevrist.currencyconverter;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    //Declare some variables
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;
    private String url = "https://api.exchangerate-api.com/v4/latest/USD";
    private String json = "";
    private String rate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //cast the variables to their ids
        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);
        //Click event
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                BackgroundTask convert = new BackgroundTask();
                convert.execute();
                //Double convert = Double.parseDouble(json);
            }
        });
    }

    private class BackgroundTask extends AsyncTask<String,Void,String> {
        //@Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);

            System.out.println(result);
            Double value = Double.parseDouble(result);

            //covert user's input to string
            usd = editText01.getText().toString();
            //if-else statement to make sure user cannot leave the EditText blank
            if (usd.equals("")){
                textView01.setText("This field cannot be blank!");
            } else {
                //Convert string to double
                Double dInputs = Double.parseDouble(usd);
                //Convert function
                Double output = dInputs * value;
                //Display the result
                textView01.setText("$" + usd + " = " + "¥"+String.format("%.2f", output));
                //clear the edittext after clicking
                editText01.setText("");
            }
        }
        //@Override
        protected String doInBackground(String... params){
            try {
                java.net.URL web_url = new URL(MainActivity.this.url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)web_url.openConnection();
                //invoke the connect() method from from the object httpURLconnection
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = bufferedReader.readLine();
                json += line;
                while(line != null){
                    //assign the BufferedReader.readline() to the string line every iteration
                    line = bufferedReader.readLine();

                    //then append the line to json
                    json += line;
                }
                System.out.println("\n JSON: " + json);
                JSONObject obj = new JSONObject(json);
                JSONObject objRate = obj.getJSONObject("rates");
                rate = objRate.get("JPY").toString();
            }
            catch(MalformedURLException e){
                e.printStackTrace();
            }
            catch(IOException e){
                e.printStackTrace();
            }
            catch(JSONException e){
                Log.e("myApp","error: unexpected JSON",e);
                System.exit(1);
            }
            return rate;
        }
    }
}